const express = require("express");
const { ApolloServer, gql } = require("apollo-server-express");
const typeDefs = require("./typeDefs");
const resolvers = require("./resolver");
// const mongoose = require("mongoose");
require("dotenv").config();
require("./db/db");

async function startServer() {
  const app = express();
  const apolloServer = new ApolloServer({
    typeDefs,
    resolvers,
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({ app: app });
  //   apolloServer.applyMiddleware({ app: app, path: "/alfa" }); // In case you want to change the path of graphql

  app.use((req, res) => {
    res.send("Hello form apoloServer");
  });

  //   await mongoose.connect("mongodb://localhost:27017/postGraphql", {
  //     useUnifiedTopology: true,
  //     useNewUrlParser: true,
  //   });

  //   console.log(`Mongoose connected !`);

  app.listen(4000, () => {
    console.log("Server is running on port ------->4000");
  });
}

startServer();
