const Post = require("./Models/post.model");

const resolvers = {
  Query: {
    hello: () => {
      return "Hello World";
    },
    // Get All Post
    getAllPosts: async () => {
      const posts = await Post.find({});
      return posts;
    },
    // Get Post by Id
    getPost: async (_parent, args, _context, _info) => {
      const { id } = args;
      return await Post.findById(id);
    },
  },
  Mutation: {
    // create Post
    createPost: async (parent, args, context, info) => {
      const { title, description } = args.post;
      const post = new Post({ title, description });
      await post.save();
      return post;
    },
    // delete Post
    deletePost: async (parent, args, context, info) => {
      const { id } = args;
      await Post.findByIdAndDelete(id);
      return "Post deleted !";
    },
    // update Post
    updatePost: async (parent, args, context, info) => {
      const { title, description } = args.post;
      const { id } = args;
      const updates = {};
      if (title !== undefined) {
        updates.title = title;
      }

      if (description !== undefined) {
        updates.description = description;
      }
      const post = await Post.findByIdAndUpdate(id, updates, { new: true });
      return post;
    },
  },
};

module.exports = resolvers;
